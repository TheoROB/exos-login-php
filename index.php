<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bienvenue</title>
</head>
<body>
    <div class="main">
    <?php 
    require "header.php";
    require "footer.php"; 
    if(isset($_SESSION['email'])){
        echo 'bonjour ' . $_SESSION['email'] .'<br/>';
        echo '<a href="logout.php?logout">logout</a>';
    }else{
        header("location:login.php");
    }
?>
    </div>
</body>
</html>
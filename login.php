<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <?php 
        require "header.php";
        require "footer.php"; 
        if(isset($_POST['connexion'])){
            if(empty($_POST['email'])){
                echo "Le champ Email est vide.";
            } else {
                if(empty($_POST['mdp'])){
                    echo "Le champ Mot de passe est vide.";
                } else {
                    $Email = htmlentities($_POST['email'], ENT_QUOTES, "UTF-8"); 
                    $MotDePasse = htmlentities($_POST['mdp'], ENT_QUOTES, "UTF-8");
                    require "connexion.php";
                    $Requete = mysqli_query($mysqli,"SELECT * FROM users WHERE email = '".$Email."' AND password = '".$MotDePasse."'");
                    if(mysqli_num_rows($Requete) == 0) {
                        echo "L'email ou le mot de passe est incorrect, le compte n'a pas été trouvé.";
                    } else {
                        $_SESSION['email'] = $Email;
                        header("location:index.php");
                        echo "Vous êtes à présent connecté !";
                    }
                }
            }
        }
    ?>
    <div class="main">    
        <form action="login.php" method="post">
            Email: <input type="text" name="email" placeholder="Entrez votre email"/><br/>
            Mot de passe: <input type="password" name="mdp" placeholder="Entrez votre mot de passe"/><br/>
            <input type="submit" name="connexion" value="Connexion"/>
        </form>
    </div>
</body>
</html>